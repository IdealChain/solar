void serial_processCommands()
{
  static char buffer[80];
  if (serial_readline(Serial.read(), buffer, 80) < 1)
    return;

  if (strncmp("RGB:", buffer, 4) == 0)
  {
    int rgb[3] = { 0 };
    int rgb_index = 0;
    char *pch = strtok(&buffer[4], ",");
    while (pch != NULL && rgb_index < 3)
    {
      rgb[rgb_index++] = atoi(pch);
      pch = strtok(NULL, ",");
    }
    Serial.println(F("RGB OK."));
    status_rgb(rgb);
  }
  
  if (strncmp("Time:", buffer, 5) == 0)
  {
    time_t t = strtoul(&buffer[5], NULL, 10);
    RTC.set(t);
    setTime(t);
    status_set_P(STATUS_OK, F("Zeit gesetzt!"));
  }
}

int serial_readline(int readch, char *buffer, int len)
{
  static int pos = 0;
  int rpos;

  if (readch > 0)
  {
    switch (readch)
    {
      case '\n': // Ignore new-lines
        break;
      case '\r': // Return on CR
        rpos = pos;
        pos = 0;  // Reset position index ready for next time
        return rpos;
      default:
        if (pos < len-1)
        {
          buffer[pos++] = readch;
          buffer[pos] = 0;
        }
    }
  }
  // No end of line has been found, so return -1.
  return -1;
}
