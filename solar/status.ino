#include <RGBTools.h>
#include "status.h"

// RGB tools config
RGBTools rgb(PIN_RGB_R, PIN_RGB_G, PIN_RGB_B, true);

status_t status_current = STATUS_OK;
uint8_t status_silent = 0;

void status_setup()
{
  // prepare IOs
  pinMode(PIN_RGB_R, OUTPUT);
  pinMode(PIN_RGB_G, OUTPUT);
  pinMode(PIN_RGB_B, OUTPUT);
  pinMode(PIN_BEEPER, OUTPUT);
}

void status_update()
{
  switch(status_current)
  {
    case STATUS_OK:
      if (status_silent)
        return;

      // fade green-white
      rgb.fadeTo(0, 75, 0, 10, 200);
      delay(100);
      rgb.fadeTo(25, 60, 60, 10, 100);
      rgb.fadeTo(0, 0, 0, 10, 200);
      break;
    case STATUS_WARN:
      // yellow
      rgb.setColor(75, 25, 0);
      delay(100);
      break;
    default:
      // red
      rgb.setColor(155, 0, 0);
      status_beep();
      delay(200);
      break;
  }

  rgb.setColor(0, 0, 0);
}

void status_rgb(int rgb[3])
{
  status_rgb(rgb[0], rgb[1], rgb[2]);
}

void status_rgb(int r, int g, int b)
{
  status_silent = (r || g || b) ? 1 : 0;
  rgb.fadeTo(r, g, b, 50, 500);
}

byte status_isOk()
{
  return status_current == STATUS_OK;
}

void status_serialTimestamp()
{
  char formattedTime[20];
  FORMAT_TIME(formattedTime, now());
  Serial.print(formattedTime);
}

void status_serialStatus()
{
  switch (status_current)
  {
    case STATUS_OK: Serial.print(F("OK")); break;
    case STATUS_WARN: Serial.print(F("WARN")); break;
    default:
      Serial.print(F("ERROR"));
  }
}

void status_set_P(status_t s, const __FlashStringHelper *msg)
{
  status_current = s;
  status_serialTimestamp();
  Serial.print(F(" "));
  status_serialStatus();

  if (msg) {
    Serial.print(F(": "));
    Serial.println(msg);
    lcd_setrow_P(msg, 0);
  }
  else
    Serial.println();
}

void status_set(status_t s, const char *msg)
{
  status_current = s;
  status_serialTimestamp();
  Serial.print(F(" "));
  status_serialStatus();

  if (msg) {
    Serial.print(F(": "));
    Serial.println(msg);
    lcd_setrow(msg, 0);
  }
  else
    Serial.println();
}

void status_beep()
{
  digitalWrite(PIN_BEEPER, HIGH);
  delay(50);
  digitalWrite(PIN_BEEPER, LOW);
}
