#ifndef STATUS_H
#define STATUS_H

// pins
#define PIN_RGB_R 5
#define PIN_RGB_G 6
#define PIN_RGB_B 7
#define PIN_BEEPER 23

typedef enum {
  STATUS_OK = 0,
  STATUS_WARN,
  STATUS_ERROR
} status_t;

#endif // STATUS_H
