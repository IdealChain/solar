// pins
#define PIN_LCD_RS 22
#define PIN_LCD_ENABLE 24
#define PIN_LCD_D4 25
#define PIN_LCD_D5 26
#define PIN_LCD_D6 27
#define PIN_LCD_D7 28
#define PIN_LCD_BACKGROUND 30

// display dimensions
const uint8_t lcd_columns = 20;
const uint8_t lcd_rows = 4;

#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(PIN_LCD_RS, PIN_LCD_ENABLE, PIN_LCD_D4, PIN_LCD_D5, PIN_LCD_D6, PIN_LCD_D7);

void lcd_setup()
{
  lcd.begin(lcd_columns, lcd_rows);
  delay(200);
  
  pinMode(PIN_LCD_BACKGROUND, OUTPUT);
}

void lcd_setrow(const char* status, uint8_t row)
{
  lcd.setCursor(0, row);
  lcd.print(status);
  
  int i = lcd_columns - strlen(status);
  while (i--)
    lcd.write(' ');
}

void lcd_setrow_P(const __FlashStringHelper *status, uint8_t row)
{ 
  lcd.setCursor(0, row);
  lcd.print(status);
  
  int i = lcd_columns - strlen_P((const char PROGMEM *)status);
  while (i--)
    lcd.write(' ');
}

void lcd_setbackground(uint8_t value) 
{
  digitalWrite(PIN_LCD_BACKGROUND, value);
}
