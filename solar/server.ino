#define WEBDUINO_FAVICON_DATA ""
#define WEBDUINO_SERIAL_DEBUGGING 2

#define FILESERVER_PORT 80
#define FILESERVER_BUFFERSIZE 1024

#include <SPI.h>
#include <Ethernet.h>
#include <WebServer.h>
#include <SD.h>

struct mime_type {
	char ending[4];
	const char *mime;
};

const struct mime_type mime_types[] = {
	{ "htm", "text/html" },
	{ "png", "image/png" },
	{ "jpg", "image/jpeg" },
	{ "css", "text/css" },
	{ "js", "text/javascript" }
};

/* forward declarations */
void server_indexCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
void server_urlCmd(WebServer &server, WebServer::ConnectionType type, char **url_path, char *url_tail, bool tail_complete);
void server_serveFile(WebServer &server, WebServer::ConnectionType type, char *file);
void server_404(WebServer &server, WebServer::ConnectionType type);

WebServer server;

void server_setup()
{
	server = WebServer("", FILESERVER_PORT);

	server.setDefaultCommand(&server_indexCmd);
	server.setUrlPathCommand(&server_urlCmd);
	server.begin();
}

void server_processConnections()
{
	char buff[64];
	int len = 64;
	
	server.processConnection(buff, &len);
}

const char *server_get_mime(const char *filename) {
	
	const char *dot = strrchr(filename, '.');
	if(!dot || dot == filename)
		return NULL;
	
	unsigned int i;
	for(i = 0; i < SIZE(mime_types); i++)
		if (strcmp(mime_types[i].ending, dot + 1) == 0)
			return mime_types[i].mime;
	
	return NULL;
}

void server_indexCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
	server_serveFile(server, type, (char*)"/");
}

void server_urlCmd(WebServer &server, WebServer::ConnectionType type, char **url_path, char *url_tail, bool tail_complete)
{
	if(!tail_complete) {
		server.httpServerError();
		return;
	}
	
	char path[64];
	unsigned int p = 0;
	
	unsigned int i = 0;
	while (url_path[i]) {
		strncpy(&path[p++], "/", sizeof(path) - p);
		strncpy(&path[p], url_path[i], sizeof(path) - p);
		p += strlen(url_path[i++]);
	}
	path[sizeof(path) - 1] = '\0';
	
	server_serveFile(server, type, path);
}

void server_404(WebServer &server, WebServer::ConnectionType type)
{
	P(failMsg404) =
		"HTTP/1.0 404 Not Found" CRLF;
	
	P(failMsgNotFound) = 
		"Content-Type: text/html" CRLF
		CRLF
		"<h1>File Not Found!</h1>";
		
	server.printP(failMsg404);
	
	#ifndef WEBDUINO_SUPRESS_SERVER_HEADER
		server.printP(webServerHeader);
	#endif
	
	server.printP(failMsgNotFound);
}

void server_serveFile(WebServer &server, WebServer::ConnectionType type, char *file)
{
	if (!SD.exists(file))
	{
#ifdef WEBDUINO_SERIAL_DEBUGGING
		Serial.print(F("File not found: "));
		Serial.println(file);
#endif
		server_404(server, type);
		return;
	}

	File f = SD.open(file, FILE_READ);
	
	if (f.isDirectory()) 
	{
		server.httpSuccess();
		server.print("<h1>Directory ");
		server.print(file);
		server.print(":</h1>");
		
		File entry = f.openNextFile();
		while (entry) {
			char e[strlen(entry.name()) * 2 + 40];
			snprintf(e, sizeof(e), "<a href=\"%s\">%s</a> (%lu)<br/>",
				entry.name(), entry.name(), entry.size());
			e[sizeof(e) - 1] = '\0';
			server.print(e);
			entry = f.openNextFile();
		}
		
		f.close();
		return;
	}  

	char clHeader[30];
	snprintf(clHeader, sizeof(clHeader), "Content-Length: %lu\r\n", f.size());
	
	const char *mime = server_get_mime(file);
	
#ifdef WEBDUINO_SERIAL_DEBUGGING
	if (mime) {
		Serial.print(F("Mimetype for "));
		Serial.print(file);
		Serial.print(F(": "));
		Serial.println(mime);
	} else {
		Serial.print(F("No mimetype for "));
		Serial.println(file);
	}
#endif
	
	if (mime)
		server.httpSuccess(mime, clHeader);
	else
		server.httpSuccess();

	if (type == WebServer::HEAD)
	{
		f.close();
		return;
	}

	char buf[FILESERVER_BUFFERSIZE];
	while(f.available())
	{
		int i = f.read(buf, sizeof(buf));

		if (server.write((uint8_t*)buf, i) != i)
			break;
	}

	f.close();
}
