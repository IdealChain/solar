// enabling DHCP increases code size by ~3 kB
// #define ETHERNET_USE_DHCP

#include <SPI.h>
#include <Ethernet.h>

// ethernet configuration
byte ethernet_mac[] = { 0x90, 0xA2, 0xDA, 0x00, 0x77, 0x20 };
IPAddress ethernet_ip(192, 168, 1, 210);
byte ethernet_online = false;

void ethernet_setup()
{
#ifdef ETHERNET_USE_DHCP
  lcd_setrow_P(F("DHCP..."), 0);
  
  if (Ethernet.begin(ethernet_mac) == 0) {
#endif
    Ethernet.begin(ethernet_mac, ethernet_ip);
#ifdef ETHERNET_USE_DHCP
  }
#endif

  ethernet_updateStatus();
}

void ethernet_updateStatus()
{
#ifdef ETHERNET_USE_DHCP
  Ethernet.maintain();
#endif

  ethernet_online = ethernet_testGateway();
}

byte ethernet_isOnline()
{
  return ethernet_online;
}

byte ethernet_testGateway()
{
  IPAddress gateway = Ethernet.gatewayIP();
 
  EthernetClient client;
  client.setTimeout(250);
  if (client.connect(gateway, 80))
  {
    client.stop();
    return true;
  }
  
  return false;
}
