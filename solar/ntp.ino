#include <TimeLib.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

// NTP configuration (http://arduino.cc/en/Tutorial/UdpNtpClient)
const char *ntp_timeServer = "ptbtime1.ptb.de"; // NTP time server
unsigned int ntp_localPort = 8888;              // local port to listen for UDP packets
const int ntp_packetSize = 48;                  // NTP timestamp is in the first 48 bytes of the message
const int ntp_timezone = +1;                    // timezone shift, relative to UTC
byte ntp_packetBuffer[ntp_packetSize];          // Buffer to hold incoming and outgoing packets

EthernetUDP Udp;

void ntp_setup()
{
  Udp.begin(ntp_localPort);
}

time_t ntp_getTime()
{
  Serial.println(F("Sending NTP packet"));
  ntp_sendPacket(ntp_timeServer); // send an NTP packet to a time server

  // wait to see if a reply is available (TODO: non-blocking solution)
  delay(1000);
  
  if (Udp.parsePacket()) {
  
    // We've received a packet, read the data from it
    Udp.read(ntp_packetBuffer, ntp_packetSize);  // read the packet into the buffer
    
    ntp_packetBuffer[16] = '\0';
    Serial.print(F("Received NTP, ID = "));
    Serial.println((char*)&ntp_packetBuffer[12]);
  
    // the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(ntp_packetBuffer[40], ntp_packetBuffer[41]);
    unsigned long lowWord = word(ntp_packetBuffer[42], ntp_packetBuffer[43]);  
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    time_t secsSince1900 = highWord << 16 | lowWord;     
   
    // subtract seventy years to convert to unix epoch
    const unsigned long seventyYears = 2208988800UL;
    time_t epoch = secsSince1900 - seventyYears;
    
    // add timezone shift
    epoch += 3600 * ntp_timezone;
    
    Serial.print(F("Unix time = "));
    Serial.println(epoch);
    return epoch;
  }
  
  Serial.println(F("No NTP response"));
  
  return 0;
}

// send an NTP request to the time server at the given address 
void ntp_sendPacket(const char *host)
{
  // set all bytes in the buffer to 0
  memset(ntp_packetBuffer, 0, ntp_packetSize); 
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  ntp_packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  ntp_packetBuffer[1] = 0;     // Stratum, or type of clock
  ntp_packetBuffer[2] = 6;     // Polling Interval
  ntp_packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  // reference ID: "1N14"
  ntp_packetBuffer[12]  = '1'; 
  ntp_packetBuffer[13]  = 'N';
  ntp_packetBuffer[14]  = '1';
  ntp_packetBuffer[15]  = '4';

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:         
  Udp.beginPacket(host, 123); //NTP requests are to port 123
  Udp.write(ntp_packetBuffer, ntp_packetSize);
  Udp.endPacket(); 
}
