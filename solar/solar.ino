// pins
#define PIN_BUTTON 3
#define PIN_SDCARD_CS 4

#include <TimeLib.h>
#include <Wire.h>
#include <MAX6900RTC.h>
#include <leOS2.h>
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>

#include "io.h"
#include "status.h"

const char TimeFormat[] PROGMEM = "%02d.%02d.%d %02d:%02d:%02d";
const char TimeFormat_ISO[] PROGMEM = "%d-%02d-%02dT%02d:%02d:%02d+01:00";

#define FORMAT_TIME(formatted, t) \
	snprintf_P(formatted, sizeof(formatted), \
	TimeFormat, \
	day(t), month(t), year(t), hour(t), minute(t), second(t))

#define FORMAT_TIME_ISO(formatted, t) \
	snprintf_P(formatted, sizeof(formatted), \
	TimeFormat_ISO, \
	year(t), month(t), day(t), hour(t), minute(t), second(t))

// create LeOS instance
leOS2 myOS;

void setup()  {
  Serial.begin(9600);
  setSyncProvider(RTC.get); // RTC as time sync provider

  status_setup();
  lcd_setup();

  lcd_setbackground(HIGH);
  status_set_P(STATUS_WARN, F("Init..."));

  // setup button
  pinMode(PIN_BUTTON, INPUT);
  digitalWrite(PIN_BUTTON, HIGH); // enable internal pull-up
  attachInterrupt(1, buttonPressed, FALLING);

  // setup sd card
  pinMode(SS, OUTPUT);
  SD.begin(PIN_SDCARD_CS);

  // setup io ports
  io_setup();

  status_set_P(STATUS_WARN, F("Network..."));

  // setup ethernet
  ethernet_setup();

  // setup ntp time sync
  ntp_setup();

  // setup fileserver
  server_setup();

  status_set_P(STATUS_WARN, F("leOS2..."));

  // initialize leOS scheduler
  myOS.begin(myOS.convertMs(8000)); // maximal time a task may take (DNS timeout: 5 secs)

  // normal tasks
  myOS.addTask(checkButton, myOS.convertMs(50));
  myOS.addTask(printStatus, myOS.convertMs(1000));
  myOS.addTask(disableLCDBackground, myOS.convertMs(10000), ONETIME);

  // long term update tasks
  myOS.addTask(ethernet_updateStatus, myOS.convertMs(30000));
  myOS.addTask(syncNTPTime, myOS.convertMs(3600 * 1000UL), SCHEDULED_IMMEDIATESTART);

  status_set_P(STATUS_OK, F("OK!"));
}

void loop()
{
  delay(50);
  server_processConnections();
  serial_processCommands();
}

volatile uint8_t buttonQueue = 0;

void buttonPressed()
{
  buttonQueue = 1;
}

void checkButton()
{
  if (!buttonQueue)
    return;

  // reset status
  status_set(STATUS_OK, NULL);

  // enable LCD background illumination, schedule disabling
  lcd_setbackground(HIGH);
  myOS.removeTask(disableLCDBackground);
  myOS.addTask(disableLCDBackground, myOS.convertMs(30000), ONETIME);

  status_beep();

  delay(250); // debounce
  buttonQueue = 0;
}

void disableLCDBackground()
{
  lcd_setbackground(LOW);
}

void printStatus()
{
  if (now() % 5 != 0)
    return;

  if (status_isOk())
    printTime();

  printEthernet();
  printFreeRam();

  status_update();
}

void printTime()
{
  if(timeStatus() != timeSet) {
    status_set_P(STATUS_ERROR, F("Time not set!"));
    return;
  }

  char formattedTime[20];
  FORMAT_TIME(formattedTime, now());

  lcd_setrow(formattedTime, 0);
}

void printEthernet()
{
  if (!ethernet_isOnline())
  {
    lcd_setrow_P(F("IP: -"), 1);
    return;
  }

  // show local IP address on display
  IPAddress ip = Ethernet.localIP();
  char formattedIP[20];
  snprintf(formattedIP, sizeof(formattedIP),
    "IP: %d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);

  lcd_setrow(formattedIP, 1);
}

void printFreeRam()
{
  extern int __heap_start, *__brkval;
  int v;
  int free = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);

  char ram[20];
  snprintf(ram, sizeof(ram), "RAM: %d/8k free", free);
  lcd_setrow(ram, 2);
}

void syncNTPTime()
{
  if (!ethernet_isOnline())
    return;

  lcd_setrow_P(F("NTP update..."), 0);
  time_t t = ntp_getTime();
  if (t > 0)
  {
    RTC.set(t); // set the RTC and the system time to the received value
    setTime(t);
  }
}
