#ifndef IO_H
#define IO_H

typedef struct {
	uint8_t portno;
	uint8_t pinmode;
	const char *name;
} io_port_t;

typedef enum {
	UNIT_NONE = 0,
	UNIT_CELSIUS
} io_unit_t;

typedef struct {
	io_port_t port;
	io_unit_t unit;
	int offset;
	float scale;
} analog_io_port_t;

#endif // IO_H