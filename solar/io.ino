#include "io.h"

// returns the number of elements in the array
#define SIZE(array) (sizeof(array) / sizeof(*array))

// tables of "payload io ports"
const io_port_t digital_io_ports[] = {
	{ LED_BUILTIN, OUTPUT, "LED13" }
};

const analog_io_port_t analog_io_ports[] = {};

void io_setup()
{
	// set pin modes
	unsigned int i;
	for(i = 0; i < SIZE(digital_io_ports); i++)
		pinMode(digital_io_ports[i].portno, digital_io_ports[i].pinmode);
	for(i = 0; i < SIZE(analog_io_ports); i++)
		pinMode(analog_io_ports[i].port.portno, analog_io_ports[i].port.pinmode);
}