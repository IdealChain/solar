# Arduino Solar control project #

This project is (was) intended to create a baseline framework for an ATmega1280 based Arduino Mega 2560 board.

Its payload would consist of controller routines for solar water heater equipment, using the numerous IO pins of the board.

### Electronic components ###

This assumes the following electronic components attached:

* Hitachi HD44780-based LCD (IO pins 22, 24-28)
* MAX6900 RTC chip (I²C port)
* W5100 Ethernet Controller (Arduino Ethernet Shield) (SPI port)
* Piezo beeper (IO pin 23)
* RGB LED (PWM IO pins 5-7, common cathode)
* A simple push button (IO pin 3, pull-down to ground on press)

### Used libraries ###

* [Arduino CMake](https://github.com/queezythegreat/arduino-cmake), cross plattform Arduino build system
* [Arduino Time library](https://github.com/PaulStoffregen/Time/) with MAX6900 RTC support
* [leOS2](https://github.com/leomil72/leOS2/) for scheduling tasks at specific intervals, based on the AVR WatchDog timer
* [RGBTools](https://github.com/IdealChain/Arduino-RGB-Tools/) for RGB fading convenience
* [WebDuino](https://github.com/sirleech/Webduino/) webserver

### How do I get set up? ###

#### Dependencies

You need to have an Arduino 1.0.5 IDE/SDK installed, which should include the necessary compiler and programming software (gcc-avr, avrdude).

For more advanced usage and better flexibility it is cool to use the arduino-cmake package, then you need to have cmake installed.

#### Building ####

* Clone repository into directory of your choice (when using the Arduino IDE, you need to set the sketchbook folder to this, defaulting to $HOME/sketchbook)

		git clone --recursive https://IdealChain@bitbucket.org/IdealChain/solar.git ~/sketchbook

* Either a) open the Arduino IDE and build the solar sketch, or 

* b) switch terminal to build folder, generate Makefiles and build

		cd ~/sketchbook/build
		cmake ..
		make

#### Uploading ####

* Edit CMakeLists.txt to set serial port and terminal options

		cd ~/sketchbook/build
		cmake ..
		make solar-upload