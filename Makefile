ifneq ("$(wildcard /usr/share/arduino/hardware/archlinux-arduino/.)","")
	PLATFORM = archlinux-arduino
else
	PLATFORM = arduino
endif

ARCH = avr
BOARD = mega
BOARD_CPU = atmega2560
FQBN = $(PLATFORM):$(ARCH):$(BOARD):cpu=$(BOARD_CPU)

SKETCH_DIR = solar
SKETCH = $(SKETCH_DIR)/solar.ino
VERBOSE = -v

.PHONY: verify upload

verify: $(SKETCH_DIR)/*
	arduino --verify --board $(FQBN) $(VERBOSE) $(SKETCH)

upload: $(SKETCH_DIR)/*
	arduino --upload --board $(FQBN) $(VERBOSE) $(SKETCH)
