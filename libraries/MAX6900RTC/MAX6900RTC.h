/*
 * MAX6900RTC.h - library for MAX6900 RTC
 * This library is intended to be uses with Arduino Time.h library functions
 */

#ifndef MAX6900RTC_h
#define MAX6900RTC_h

#include <TimeLib.h>

// library interface description
class MAX6900RTC
{
  // user-accessible "public" interface
  public:
    MAX6900RTC();
    static time_t get();
	static void set(time_t t);
	static void read(tmElements_t &tm);
	static void write(tmElements_t &tm);

  private:
	static uint8_t dec2bcd(uint8_t num);
    static uint8_t bcd2dec(uint8_t num);
};

extern MAX6900RTC RTC;

#endif


